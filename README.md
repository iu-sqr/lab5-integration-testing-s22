# Lab5 -- Integration testing


## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab
Key for spring semester in 2022 is `AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w`

Ok, finally, we won't need to write any code today :). Hope you're happy)
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab5-integration-testing)
2. Try to use InnoDrive application:
- At first we need to check our default parameters, for it we will need some REST client preferrably(Insomnia, Postman or you may use your browser), install it. 
- Then let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars `budget` and `luxury`. The service offers flexible two tariff plans. With the `minute` plan, the service charges its client by `time per minute of use`. The Service also proposes a `fixed_price` plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than `n%`, the tariff plan automatically switches to the `minute` plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The `fixed_price` plan is available for `budget` cars only. The Innopolis city sponsors the mobility and offers `m%` discount to Innopolis residents.
    Imagine you are the quality engineer of Innodrive. How are you going to know about application quality???
- InnoDrive application is just a web application that allows you to compute the price for a certain car ride
- To get particularly your defaults, let's send this request:
    `https://script.google.com/macros/s/_your_key_/exec?service=getSpec&email=_your_email_` 
  This will return our default spec(your result might be different):
        Here is InnoCar Specs:

        Budet car price per minute = 17
        
        Luxury car price per minute = 33
        
        Fixed price per km = 11
        
        Allowed deviations in % = 10
        
        Inno discount in % = 10

- To get the price for certain car ride, let's send next request:
`https://script.google.com/macros/s/_your_key_/exec?service=calculatePrice&email=_your_email_&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_` with some random parameters. Answer should be like `{"price": 100}` or `Invalid Request`
- And next we can create our BVA table, this is a lab, so I will create it just for 2 parameters: 
`distance` and `type`.
 `distance` is an integer value, so we can build 2 equivalence classes:
 + `distance` <= 0
 + `distance` > 0
 while `type` have 3 equivalence classes:
 + `budget` 
 + `luxury`
 + or some `nonsense`.

- Let's use BVA to make integration tests,we need to make few different testcases with `distance`, depending on its value: 
 + `distance` <= 0 : -10 0
 + `distance` > 0 : 1 100000
This way we will test both our borders and our normal values, we can do the same thing for `type`:
 + `type` = "budget" 
 + `type` = "luxury" 
 + `type` = "Guten morgen sonnenschein" 

- Now, let's check responses for different values of our parameters, with all other parameters already setted up, so we will have:
  + `plan` = `minute` 
  + `planned_distance` = `100` 
  + `time` = `110` 
  + `planned_time` = `100` 
  + `inno_discount` = `yes`
  Let's build test cases out of this data:

| Test case  | distance |   type    | Expected result      |
|------------|----------|-----------|----------------------|
|     1      |    -10   |     *     |   Invalid Request    |
|     2      |    0     |     *     |   Invalid Request    |
|     3      |    *     | "nonsense"|   Invalid Request    |
|     4      |    1     |"budget"   |   1683               |
|     5      |    1     |"luxury"   |   3267               |
|     6      | 1000000  |"budget"   |   1683               |
|     7      | 1000000  |"luxury"   |   3267               |

etc...(there are a lot of combinations), to pick only needed one it is a good practice to use specialized tools.
Let's use Decision table to cut out our tests:

| Conditions(inputs)  |             Values           |    R1    |   R2    |   R3  |    R4   |
|---------------------|------------------------------|----------|---------|-------|---------|
|     Type            | "budget","luxury", nonsense  | nonsense |  budget | luxury|    *    |
|     Distance        | >0, <=0                      |     *    |  >0     |  >0   |    <=0  |
|---------------------|------------------------------|----------|---------|-------|---------|
|  Invalid Request    |                              |    X     |         |       |    X    |
|     200             |                              |          |    X    |   X   |         |

Now let's use this request to test our values




## Homework
As a homework you will need to develop BVA and Decision table for your service, guaranteed that your service has a bug somewhere(maybe even few), you need to catch'em all using BVA. Submit your catched bugs and Tables, adding them to the Readme of your branch. 

### BVA

| Parameter          | Equivalence Classes                 |
|--------------------|-------------------------------------|
| `type`             | `budget`, `luxury`, `nonsense`      |
| `plan`             | `minute`, `fixed_price`, `nonsense` |
| `distance`         | `<=0`, `>0`                         |
| `planned_distance` | `<=0`, `>0`                         |
| `time`             | `<=0`, `>0`                         |
| `planned_time`     | `<=0`, `>0`                         |
| `inno_discount`    | `yes`, `no`, `nonsense`             |

In addition to classes above, numeric values could include class separation based on its memory size to check for over- or under-flow, but I decided not to include it, since the API was still operating with numbers like `2^512`.

### Decision table

| Conditions (inputs) | Values                              |  R1   |  R2   |  R3   |  R4   |     R5     |     R6     |     R7     |      R8       |    R9    |   R10    |      R11      |      R12      |   R13    |   R14    |
| ------------------- | ----------------------------------- |:-----:|:-----:|:-----:|:-----:|:----------:|:----------:|:----------:|:-------------:|:--------:|:--------:|:-------------:|:-------------:|:--------:|:--------:|
| `distance`          | `>0`, `<=0`                         | `<=0` | `>0`  | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `planned_distance`  | `>0`, `<=0`                         |   *   | `<=0` | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `time`              | `>0`, `<=0`                         |   *   |   *   | `<=0` | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `planned_time`      | `>0`, `<=0`                         |   *   |   *   |   *   | `<=0` |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `type`              | `budget`, `luxury`, `nonsense`      |   *   |   *   |   *   |   *   | `nonsense` |     *      |     *      |   `luxury`    | `budget` | `budget` |   `budget`    |   `budget`    | `luxury` | `luxury` |
| `plan`              | `minute`, `fixed_price`, `nonsense` |   *   |   *   |   *   |   *   |     *      | `nonsense` |     *      | `fixed_price` | `minute` | `minute` | `fixed_price` | `fixed_price` | `minute` | `minute` |
| `inno_discount`     | `yes`, `no`, `nonsense`             |   *   |   *   |   *   |   *   |     *      |     *      | `nonsense` |       *       |   `no`   |  `yes`   |     `no`      |     `yes`     |   `no`   |  `yes`   |
| **Results**         |                                     |       |       |       |       |            |            |            |               |          |          |               |               |          |          |
| Invalid Request     |                                     |   X   |   X   |   X   |   X   |     X      |     X      |     X      |       X       |          |          |               |               |          |          |
| 200                 |                                     |       |       |       |       |            |            |            |               |    X     |    X     |       X       |       X       |    X     |    X     |


### Test cases

**Disclaimer:** since the method accepts a parameter, I expect the endpoint to validate it, even if the parameter remains unused

Test cases below are generated based on the decision table using [permutator.py](permutator.py) script.
<details>
  <summary>How to run the script</summary>
  !! Script is tested on python 3.9 !!

  1. install dependencies: `pip install -r requirements.txt`
  
  2. run the script: `python3.9 permutator.py`
  
</details>

The script expands the decision table by doing the following substitutes:
* `>0`: each of [1, 42]
* `<=0`: each of [-49.5, 0]
* `*`: the first available value
* other values are left as is

The list of concluded bugs is below the table

|   idx | DT entry   |   distance |   planned_distance |   time |   planned_time | type     | plan        | inno_discount   | expected           | actual             |
|-------|------------|------------|--------------------|--------|----------------|----------|-------------|-----------------|--------------------|--------------------|
|     0 | R1         |      -49.5 |                1   |    1   |            1   | budget   | minute      | yes             | Invalid Request    | 20                 |
|     1 | R1         |        0   |                1   |    1   |            1   | budget   | minute      | yes             | Invalid Request    | 20                 |
|     2 | R2         |        1   |              -49.5 |    1   |            1   | budget   | minute      | yes             | Invalid Request    | 20                 |
|     3 | R2         |        1   |                0   |    1   |            1   | budget   | minute      | yes             | Invalid Request    | 20                 |
|     4 | R2         |       42   |              -49.5 |    1   |            1   | budget   | minute      | yes             | Invalid Request    | 20                 |
|     5 | R2         |       42   |                0   |    1   |            1   | budget   | minute      | yes             | Invalid Request    | 20                 |
|     6 | R3         |        1   |                1   |  -49.5 |            1   | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|     7 | R3         |        1   |                1   |    0   |            1   | budget   | minute      | yes             | Invalid Request    | 0                  |
|     8 | R3         |        1   |               42   |  -49.5 |            1   | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|     9 | R3         |        1   |               42   |    0   |            1   | budget   | minute      | yes             | Invalid Request    | 0                  |
|    10 | R3         |       42   |                1   |  -49.5 |            1   | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    11 | R3         |       42   |                1   |    0   |            1   | budget   | minute      | yes             | Invalid Request    | 0                  |
|    12 | R3         |       42   |               42   |  -49.5 |            1   | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    13 | R3         |       42   |               42   |    0   |            1   | budget   | minute      | yes             | Invalid Request    | 0                  |
|    14 | R4         |        1   |                1   |    1   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    15 | R4         |        1   |                1   |    1   |            0   | budget   | minute      | yes             | Invalid Request    | 20                 |
|    16 | R4         |        1   |                1   |   42   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    17 | R4         |        1   |                1   |   42   |            0   | budget   | minute      | yes             | Invalid Request    | 840                |
|    18 | R4         |        1   |               42   |    1   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    19 | R4         |        1   |               42   |    1   |            0   | budget   | minute      | yes             | Invalid Request    | 20                 |
|    20 | R4         |        1   |               42   |   42   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    21 | R4         |        1   |               42   |   42   |            0   | budget   | minute      | yes             | Invalid Request    | 840                |
|    22 | R4         |       42   |                1   |    1   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    23 | R4         |       42   |                1   |    1   |            0   | budget   | minute      | yes             | Invalid Request    | 20                 |
|    24 | R4         |       42   |                1   |   42   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    25 | R4         |       42   |                1   |   42   |            0   | budget   | minute      | yes             | Invalid Request    | 840                |
|    26 | R4         |       42   |               42   |    1   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    27 | R4         |       42   |               42   |    1   |            0   | budget   | minute      | yes             | Invalid Request    | 20                 |
|    28 | R4         |       42   |               42   |   42   |          -49.5 | budget   | minute      | yes             | Invalid Request    | Invalid Request    |
|    29 | R4         |       42   |               42   |   42   |            0   | budget   | minute      | yes             | Invalid Request    | 840                |
|    30 | R5         |        1   |                1   |    1   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    31 | R5         |        1   |                1   |    1   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    32 | R5         |        1   |                1   |   42   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    33 | R5         |        1   |                1   |   42   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    34 | R5         |        1   |               42   |    1   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    35 | R5         |        1   |               42   |    1   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    36 | R5         |        1   |               42   |   42   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    37 | R5         |        1   |               42   |   42   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    38 | R5         |       42   |                1   |    1   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    39 | R5         |       42   |                1   |    1   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    40 | R5         |       42   |                1   |   42   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    41 | R5         |       42   |                1   |   42   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    42 | R5         |       42   |               42   |    1   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    43 | R5         |       42   |               42   |    1   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    44 | R5         |       42   |               42   |   42   |            1   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    45 | R5         |       42   |               42   |   42   |           42   | nonsense | minute      | yes             | Invalid Request    | Invalid Request    |
|    46 | R6         |        1   |                1   |    1   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    47 | R6         |        1   |                1   |    1   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    48 | R6         |        1   |                1   |   42   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    49 | R6         |        1   |                1   |   42   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    50 | R6         |        1   |               42   |    1   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    51 | R6         |        1   |               42   |    1   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    52 | R6         |        1   |               42   |   42   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    53 | R6         |        1   |               42   |   42   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    54 | R6         |       42   |                1   |    1   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    55 | R6         |       42   |                1   |    1   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    56 | R6         |       42   |                1   |   42   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    57 | R6         |       42   |                1   |   42   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    58 | R6         |       42   |               42   |    1   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    59 | R6         |       42   |               42   |    1   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    60 | R6         |       42   |               42   |   42   |            1   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    61 | R6         |       42   |               42   |   42   |           42   | budget   | nonsense    | yes             | Invalid Request    | Invalid Request    |
|    62 | R7         |        1   |                1   |    1   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    63 | R7         |        1   |                1   |    1   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    64 | R7         |        1   |                1   |   42   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    65 | R7         |        1   |                1   |   42   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    66 | R7         |        1   |               42   |    1   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    67 | R7         |        1   |               42   |    1   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    68 | R7         |        1   |               42   |   42   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    69 | R7         |        1   |               42   |   42   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    70 | R7         |       42   |                1   |    1   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    71 | R7         |       42   |                1   |    1   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    72 | R7         |       42   |                1   |   42   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    73 | R7         |       42   |                1   |   42   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    74 | R7         |       42   |               42   |    1   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    75 | R7         |       42   |               42   |    1   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    76 | R7         |       42   |               42   |   42   |            1   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    77 | R7         |       42   |               42   |   42   |           42   | budget   | minute      | nonsense        | Invalid Request    | Invalid Request    |
|    78 | R8         |        1   |                1   |    1   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    79 | R8         |        1   |                1   |    1   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    80 | R8         |        1   |                1   |   42   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    81 | R8         |        1   |                1   |   42   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    82 | R8         |        1   |               42   |    1   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    83 | R8         |        1   |               42   |    1   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    84 | R8         |        1   |               42   |   42   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    85 | R8         |        1   |               42   |   42   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    86 | R8         |       42   |                1   |    1   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    87 | R8         |       42   |                1   |    1   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    88 | R8         |       42   |                1   |   42   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    89 | R8         |       42   |                1   |   42   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    90 | R8         |       42   |               42   |    1   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    91 | R8         |       42   |               42   |    1   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    92 | R8         |       42   |               42   |   42   |            1   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    93 | R8         |       42   |               42   |   42   |           42   | luxury   | fixed_price | yes             | Invalid Request    | Invalid Request    |
|    94 | R9         |        1   |                1   |    1   |            1   | budget   | minute      | no              | 18                 | 20                 |
|    95 | R9         |        1   |                1   |    1   |           42   | budget   | minute      | no              | 18                 | 20                 |
|    96 | R9         |        1   |                1   |   42   |            1   | budget   | minute      | no              | 756                | 840                |
|    97 | R9         |        1   |                1   |   42   |           42   | budget   | minute      | no              | 756                | 840                |
|    98 | R9         |        1   |               42   |    1   |            1   | budget   | minute      | no              | 18                 | 20                 |
|    99 | R9         |        1   |               42   |    1   |           42   | budget   | minute      | no              | 18                 | 20                 |
|   100 | R9         |        1   |               42   |   42   |            1   | budget   | minute      | no              | 756                | 840                |
|   101 | R9         |        1   |               42   |   42   |           42   | budget   | minute      | no              | 756                | 840                |
|   102 | R9         |       42   |                1   |    1   |            1   | budget   | minute      | no              | 18                 | 20                 |
|   103 | R9         |       42   |                1   |    1   |           42   | budget   | minute      | no              | 18                 | 20                 |
|   104 | R9         |       42   |                1   |   42   |            1   | budget   | minute      | no              | 756                | 840                |
|   105 | R9         |       42   |                1   |   42   |           42   | budget   | minute      | no              | 756                | 840                |
|   106 | R9         |       42   |               42   |    1   |            1   | budget   | minute      | no              | 18                 | 20                 |
|   107 | R9         |       42   |               42   |    1   |           42   | budget   | minute      | no              | 18                 | 20                 |
|   108 | R9         |       42   |               42   |   42   |            1   | budget   | minute      | no              | 756                | 840                |
|   109 | R9         |       42   |               42   |   42   |           42   | budget   | minute      | no              | 756                | 840                |
|   110 | R10        |        1   |                1   |    1   |            1   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   111 | R10        |        1   |                1   |    1   |           42   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   112 | R10        |        1   |                1   |   42   |            1   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   113 | R10        |        1   |                1   |   42   |           42   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   114 | R10        |        1   |               42   |    1   |            1   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   115 | R10        |        1   |               42   |    1   |           42   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   116 | R10        |        1   |               42   |   42   |            1   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   117 | R10        |        1   |               42   |   42   |           42   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   118 | R10        |       42   |                1   |    1   |            1   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   119 | R10        |       42   |                1   |    1   |           42   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   120 | R10        |       42   |                1   |   42   |            1   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   121 | R10        |       42   |                1   |   42   |           42   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   122 | R10        |       42   |               42   |    1   |            1   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   123 | R10        |       42   |               42   |    1   |           42   | budget   | minute      | yes             | 14.760000000000002 | 20                 |
|   124 | R10        |       42   |               42   |   42   |            1   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   125 | R10        |       42   |               42   |   42   |           42   | budget   | minute      | yes             | 619.9200000000001  | 840                |
|   126 | R11        |        1   |                1   |    1   |            1   | budget   | fixed_price | no              | 10                 | 12.5               |
|   127 | R11        |        1   |                1   |    1   |           42   | budget   | fixed_price | no              | 18                 | 16.666666666666668 |
|   128 | R11        |        1   |                1   |   42   |            1   | budget   | fixed_price | no              | 756                | 700                |
|   129 | R11        |        1   |                1   |   42   |           42   | budget   | fixed_price | no              | 10                 | 12.5               |
|   130 | R11        |        1   |               42   |    1   |            1   | budget   | fixed_price | no              | 18                 | 16.666666666666668 |
|   131 | R11        |        1   |               42   |    1   |           42   | budget   | fixed_price | no              | 18                 | 16.666666666666668 |
|   132 | R11        |        1   |               42   |   42   |            1   | budget   | fixed_price | no              | 756                | 700                |
|   133 | R11        |        1   |               42   |   42   |           42   | budget   | fixed_price | no              | 756                | 700                |
|   134 | R11        |       42   |                1   |    1   |            1   | budget   | fixed_price | no              | 18                 | 16.666666666666668 |
|   135 | R11        |       42   |                1   |    1   |           42   | budget   | fixed_price | no              | 18                 | 16.666666666666668 |
|   136 | R11        |       42   |                1   |   42   |            1   | budget   | fixed_price | no              | 756                | 700                |
|   137 | R11        |       42   |                1   |   42   |           42   | budget   | fixed_price | no              | 756                | 700                |
|   138 | R11        |       42   |               42   |    1   |            1   | budget   | fixed_price | no              | 420                | 525                |
|   139 | R11        |       42   |               42   |    1   |           42   | budget   | fixed_price | no              | 18                 | 16.666666666666668 |
|   140 | R11        |       42   |               42   |   42   |            1   | budget   | fixed_price | no              | 756                | 700                |
|   141 | R11        |       42   |               42   |   42   |           42   | budget   | fixed_price | no              | 420                | 525                |
|   142 | R12        |        1   |                1   |    1   |            1   | budget   | fixed_price | yes             | 8.200000000000001  | 12.5               |
|   143 | R12        |        1   |                1   |    1   |           42   | budget   | fixed_price | yes             | 14.760000000000002 | 16.666666666666668 |
|   144 | R12        |        1   |                1   |   42   |            1   | budget   | fixed_price | yes             | 619.9200000000001  | 700                |
|   145 | R12        |        1   |                1   |   42   |           42   | budget   | fixed_price | yes             | 8.200000000000001  | 12.5               |
|   146 | R12        |        1   |               42   |    1   |            1   | budget   | fixed_price | yes             | 14.760000000000002 | 16.666666666666668 |
|   147 | R12        |        1   |               42   |    1   |           42   | budget   | fixed_price | yes             | 14.760000000000002 | 16.666666666666668 |
|   148 | R12        |        1   |               42   |   42   |            1   | budget   | fixed_price | yes             | 619.9200000000001  | 700                |
|   149 | R12        |        1   |               42   |   42   |           42   | budget   | fixed_price | yes             | 619.9200000000001  | 700                |
|   150 | R12        |       42   |                1   |    1   |            1   | budget   | fixed_price | yes             | 14.760000000000002 | 16.666666666666668 |
|   151 | R12        |       42   |                1   |    1   |           42   | budget   | fixed_price | yes             | 14.760000000000002 | 16.666666666666668 |
|   152 | R12        |       42   |                1   |   42   |            1   | budget   | fixed_price | yes             | 619.9200000000001  | 700                |
|   153 | R12        |       42   |                1   |   42   |           42   | budget   | fixed_price | yes             | 619.9200000000001  | 700                |
|   154 | R12        |       42   |               42   |    1   |            1   | budget   | fixed_price | yes             | 344.40000000000003 | 525                |
|   155 | R12        |       42   |               42   |    1   |           42   | budget   | fixed_price | yes             | 14.760000000000002 | 16.666666666666668 |
|   156 | R12        |       42   |               42   |   42   |            1   | budget   | fixed_price | yes             | 619.9200000000001  | 700                |
|   157 | R12        |       42   |               42   |   42   |           42   | budget   | fixed_price | yes             | 344.40000000000003 | 525                |
|   158 | R13        |        1   |                1   |    1   |            1   | luxury   | minute      | no              | 40                 | 40                 |
|   159 | R13        |        1   |                1   |    1   |           42   | luxury   | minute      | no              | 40                 | 40                 |
|   160 | R13        |        1   |                1   |   42   |            1   | luxury   | minute      | no              | 1680               | 1680               |
|   161 | R13        |        1   |                1   |   42   |           42   | luxury   | minute      | no              | 1680               | 1680               |
|   162 | R13        |        1   |               42   |    1   |            1   | luxury   | minute      | no              | 40                 | 40                 |
|   163 | R13        |        1   |               42   |    1   |           42   | luxury   | minute      | no              | 40                 | 40                 |
|   164 | R13        |        1   |               42   |   42   |            1   | luxury   | minute      | no              | 1680               | 1680               |
|   165 | R13        |        1   |               42   |   42   |           42   | luxury   | minute      | no              | 1680               | 1680               |
|   166 | R13        |       42   |                1   |    1   |            1   | luxury   | minute      | no              | 40                 | 40                 |
|   167 | R13        |       42   |                1   |    1   |           42   | luxury   | minute      | no              | 40                 | 40                 |
|   168 | R13        |       42   |                1   |   42   |            1   | luxury   | minute      | no              | 1680               | 1680               |
|   169 | R13        |       42   |                1   |   42   |           42   | luxury   | minute      | no              | 1680               | 1680               |
|   170 | R13        |       42   |               42   |    1   |            1   | luxury   | minute      | no              | 40                 | 40                 |
|   171 | R13        |       42   |               42   |    1   |           42   | luxury   | minute      | no              | 40                 | 40                 |
|   172 | R13        |       42   |               42   |   42   |            1   | luxury   | minute      | no              | 1680               | 1680               |
|   173 | R13        |       42   |               42   |   42   |           42   | luxury   | minute      | no              | 1680               | 1680               |
|   174 | R14        |        1   |                1   |    1   |            1   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   175 | R14        |        1   |                1   |    1   |           42   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   176 | R14        |        1   |                1   |   42   |            1   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |
|   177 | R14        |        1   |                1   |   42   |           42   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |
|   178 | R14        |        1   |               42   |    1   |            1   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   179 | R14        |        1   |               42   |    1   |           42   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   180 | R14        |        1   |               42   |   42   |            1   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |
|   181 | R14        |        1   |               42   |   42   |           42   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |
|   182 | R14        |       42   |                1   |    1   |            1   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   183 | R14        |       42   |                1   |    1   |           42   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   184 | R14        |       42   |                1   |   42   |            1   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |
|   185 | R14        |       42   |                1   |   42   |           42   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |
|   186 | R14        |       42   |               42   |    1   |            1   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   187 | R14        |       42   |               42   |    1   |           42   | luxury   | minute      | yes             | 32.800000000000004 | 40                 |
|   188 | R14        |       42   |               42   |   42   |            1   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |
|   189 | R14        |       42   |               42   |   42   |           42   | luxury   | minute      | yes             | 1377.6000000000001 | 1680               |

### Concluded bugs
The spec for me is:
* Budet car price per minute = 18
* Luxury car price per minute = 40
* Fixed price per km = 10
* Allowed deviations in % = 15
* Inno discount in % = 18

![img.png](img.png)


Bug list (I will list only up to 5 confirming cases)
1) cases 0-5: code does not check `distance` and `planned_distance` > 0 for `minute` rides;
1) cases 7, 9, 11, 13: 0 minute timed rides are considered ok;
1) cases 15, 17, 19, 21, 23: `minute` rides with 0 `planned_distance` are considered ok;
1) cases 94-109: `budget` car per `minute` price is 20 instead of 18;
1) cases 110-125: inno discount does not work;
1) cases 126-141: `budget` car per `km` price is 12.5 instead of 10. 
   Probably deviation mechanism is also broken. Hard to tell, since prices do not add up;
1) cases 143-157: inno discount does not work + incorrect price per km for `budget` cars;
1) cases 174-189: inno discount does not apply.
