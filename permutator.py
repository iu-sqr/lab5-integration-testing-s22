from concurrent.futures import ThreadPoolExecutor
from dataclasses import dataclass
from functools import partial
from itertools import chain
from typing import Dict, Any

import tabulate
from requests import Session

API_KEY = "AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w"
BASE_URL = f"https://script.google.com/macros/s/{API_KEY}/exec"
EMAIL = "d.manakovskiy@innopolis.university"
CALCULATION_SERVICE = "calculatePrice"

STATUSES_IN_TABLE = 2  # In this table: Invalid Request, 200
DECISION_TABLE_MD = """| Conditions (inputs) | Values                              |  R1   |  R2   |  R3   |  R4   |     R5     |     R6     |     R7     |      R8       |    R9    |   R10    |      R11      |      R12      |   R13    |   R14    |
| ------------------- | ----------------------------------- |:-----:|:-----:|:-----:|:-----:|:----------:|:----------:|:----------:|:-------------:|:--------:|:--------:|:-------------:|:-------------:|:--------:|:--------:|
| `distance`          | `>0`, `<=0`                         | `<=0` | `>0`  | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `planned_distance`  | `>0`, `<=0`                         |   *   | `<=0` | `>0`  | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `time`              | `>0`, `<=0`                         |   *   |   *   | `<=0` | `>0`  |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `planned_time`      | `>0`, `<=0`                         |   *   |   *   |   *   | `<=0` |    `>0`    |    `>0`    |    `>0`    |     `>0`      |   `>0`   |   `>0`   |     `>0`      |     `>0`      |   `>0`   |   `>0`   |
| `type`              | `budget`, `luxury`, `nonsense`      |   *   |   *   |   *   |   *   | `nonsense` |     *      |     *      |   `luxury`    | `budget` | `budget` |   `budget`    |   `budget`    | `luxury` | `luxury` |
| `plan`              | `minute`, `fixed_price`, `nonsense` |   *   |   *   |   *   |   *   |     *      | `nonsense` |     *      | `fixed_price` | `minute` | `minute` | `fixed_price` | `fixed_price` | `minute` | `minute` |
| `inno_discount`     | `yes`, `no`, `nonsense`             |   *   |   *   |   *   |   *   |     *      |     *      | `nonsense` |       *       |   `no`   |  `yes`   |     `no`      |     `yes`     |   `no`   |  `yes`   |
| **Results**         |                                     |       |       |       |       |            |            |            |               |          |          |               |               |          |          |
| Invalid Request     |                                     |   X   |   X   |   X   |   X   |     X      |     X      |     X      |       X       |          |          |               |               |          |          |
| 200                 |                                     |       |       |       |       |            |            |            |               |    X     |    X     |       X       |       X       |    X     |    X     |
""".strip()

GENERATOR_VALUES = {
    "<=0": (-49.5, 0),
    ">0": (1, 42),
}


@dataclass
class Config:
    budget_car_price_per_minute: int
    luxary_car_price_per_minute: int
    fixed_price_per_km: int
    allowed_deviation_rate: float
    inno_discount_rate: float


def is_valid_request(params: Dict[str, Any]) -> bool:
    in_params = lambda keys: all(map(lambda x: x in params, keys))
    main_params_present = in_params(["type", "plan", "inno_discount"])
    unit_params_present = in_params(["time", "planned_time"]) ^ in_params(["distance", "planned_distance"])

    if not (main_params_present and unit_params_present):
        return False

    # budget type -implies-> fixed price
    return params["type"] != "budget" or params["plan"] == "fixed_price"


def parse_markdown_table(md):
    # https://stackoverflow.com/a/66185947
    lines = md.split("\n")
    ret = []
    keys = []
    for i, l in enumerate(lines):
        if i == 0:
            keys = [_i.strip() for _i in l.split("|")]
        elif i == 1:
            continue
        else:
            ret.append(
                {
                    keys[_i]: v.strip()
                    for _i, v in enumerate(l.split("|"))
                    if 0 < _i < len(keys) - 1
                }
            )

    return ret


def parse_markdown_decision_table(md):
    table = parse_markdown_table(md.replace("`", ""))
    matrix = list(
        filter(
            lambda lst: "Results" not in lst[0],
            (list(x.values()) for x in table),
        )
    )
    transpose = list(zip(*matrix))

    keys, possible_values, values = transpose[0], transpose[1], transpose[2:]
    zipped_data = [tuple(zip(keys, combination)) for combination in values]

    def get_expected_status(statuses):
        return [resp[0] for resp in statuses if resp[1]][0]

    decisions = [
        (
            dict(args[:-STATUSES_IN_TABLE]),
            get_expected_status(args[-STATUSES_IN_TABLE:]),
        )
        for args in zipped_data
    ]
    possible_generators_mapped = {
        k: v.replace(" ", "").split(",")
        for k, v in zip(keys[:-STATUSES_IN_TABLE], possible_values[:-STATUSES_IN_TABLE])
    }
    return decisions, possible_generators_mapped


def expand_condition(condition, known_generators):
    for key, generator in condition.items():
        used_generator = known_generators[key][0] if generator == "*" else generator

        generator_values = GENERATOR_VALUES.get(used_generator, [used_generator])

        if generator == "*":
            generator_values = generator_values[:1]

        if len(generator_values) == 1:
            condition[key] = generator_values[0]
        else:
            ans = []
            for value in generator_values:
                new_condition = condition.copy()
                new_condition[key] = value
                ans.extend(expand_condition(new_condition, known_generators))
            return ans

    return [condition]


def calculate_expected_price(config: Config, params) -> float:
    price_per_minute = (
        config.budget_car_price_per_minute
        if params["type"] == "budget"
        else config.luxary_car_price_per_minute
    )
    price_per_km = config.fixed_price_per_km if params["type"] == "budget" else None

    price_modifier = (
        1 - config.inno_discount_rate if params["inno_discount"] == "yes" else 1
    )

    if params["plan"] == "minute":
        return price_per_minute * params["time"] * price_modifier
    elif params["plan"] == "fixed_price":
        planned_distance = params["planned_distance"]
        actual_distance = params["distance"]

        planned_time = params["planned_time"]
        actual_time = params["time"]

        deviation = max(
            abs(actual_distance - planned_distance) / planned_distance,
            abs(actual_time - planned_time) / planned_time,
        )

        if deviation > config.allowed_deviation_rate:
            new_params = params.copy()
            new_params["plan"] = "minute"
            return calculate_expected_price(config, new_params)

        return planned_distance * price_per_km * price_modifier


def validate_request(config: Config, session: Session, request_description):
    decision_number, params, expected_outcome = request_description

    resp = session.get(BASE_URL, params=params)
    actual_output = resp.text

    if resp.ok and resp.text != "Invalid Request":
        actual_output = resp.json()["price"]

    expected_result = (
        calculate_expected_price(config, params)
        if expected_outcome != "Invalid Request"
        else expected_outcome
    )

    return chain([decision_number], params.values(), [expected_result, actual_output])


def main():
    config = Config(
        budget_car_price_per_minute=18,
        luxary_car_price_per_minute=40,
        fixed_price_per_km=10,
        allowed_deviation_rate=0.15,
        inno_discount_rate=0.18,
    )

    decisions, known_generators = parse_markdown_decision_table(DECISION_TABLE_MD)
    request_descriptions = [
        (f"R{idx + 1}", params, expected_outcome)
        for idx, (condition, expected_outcome) in enumerate(decisions)
        for params in expand_condition(condition, known_generators)
    ]

    session = Session()
    session.params.update(
        {
            "email": EMAIL,
            "service": CALCULATION_SERVICE,
        }
    )

    verified_data = []
    with ThreadPoolExecutor(max_workers=16) as executor:
        for idx, verification_result in enumerate(
            executor.map(
                partial(validate_request, config, session),
                request_descriptions,
            )
        ):
            verified_data.append((idx,) + tuple(verification_result))

    print(
        tabulate.tabulate(
            verified_data,
            headers=chain(
                ["idx", "DT entry"], known_generators.keys(), ["expected", "actual"]
            ),
            tablefmt="github",
        )
    )


if __name__ == "__main__":
    main()
